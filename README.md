# Qlimax 2023 Live Broadcast

Data files related to live broadcast, that took place on November 18

The project contains dirs with sfv, nfo & jpg files inside

## Saturday, November 18

**MAIN**

- Adjuzt & Purge
- B-Front & Phuture Noize
- Blademasterz
- Cryex
- Deadly Guns
- Deepack
- Devin Wild
- Hard Driver
- Showtek
- The Qreator
- TNT
- Vertile
- Warface

## Video content

All video content can be downloaded from [1gabba.pw](https://1gabba.pw):

- Qlimax.2023.Adjuzt.The.Purge.1080p.WEB.x264
- Qlimax.2023.B-Front.Phuture.Noize.1080p.WEB.x264
- Qlimax.2023.Blademasterz.1080p.WEB.x264
- Qlimax.2023.Cryex.1080p.WEB.x264
- Qlimax.2023.Deadly.Guns.1080p.WEB.x264
- Qlimax.2023.Deepack.1080p.WEB.x264
- Qlimax.2023.Devin.Wild.1080p.WEB.x264
- Qlimax.2023.Hard.Driver.1080p.WEB.x264
- Qlimax.2023.Showtek.1080p.WEB.x264
- Qlimax.2023.The.Qreator.1080p.WEB.x264
- Qlimax.2023.TNT.1080p.WEB.x264
- Qlimax.2023.Vertile.1080p.WEB.x264
- Qlimax.2023.Warface.1080p.WEB.x264

## Audio content

All audio content can be downloaded from [1gabba.pw](https://1gabba.pw):
- Adjuzt_and_Purge_-_Qlimax_2023-WEB-2023
- B-Front_and_Phuture_Noize_-_Qlimax_2023-WEB-2023
- Blademasterz_-_Qlimax_2023-WEB-2023
- Cryex_-_Qlimax_2023-WEB-2023
- Deadly_Guns_-_Qlimax_2023-WEB-2023
- Deepack_-_Qlimax_2023-WEB-2023
- Devin_Wild_-_Qlimax_2023-WEB-2023
- Hard_Driver_-_Qlimax_2023-WEB-2023
- Showtek_-_Qlimax_2023-WEB-2023
- The_Qreator_-_Qlimax_2023-WEB-2023
- TNT_-_Qlimax_2023-WEB-2023
- Vertile_-_Qlimax_2023-WEB-2023
- Warface_-_Qlimax_2023-WEB-2023

***
